# Checks to see if url matches last downloaded picture and downloads new picture if it is different.

include Sidekiq::Worker
require 'mechanize'
x = 0
while x < $instagram_pictures do
    next if Instagram.user_recent_media("1470967280", {:count => $instagram_pictures})[x]["images"].nil?
    instagram = Instagram.user_recent_media("1470967280", {:count => $instagram_pictures})[x]["images"]["standard_resolution"]["url"]
    agent = Mechanize.new
    agent.get(instagram).save! "app/assets/images/instagram#{x}.jpg"
    x += 1
    end

Sidekiq::Cron::Job.create(name: 'Check for new Instagram Pics every 5 min', cron: '*/5 * * * *', class: 'InstagramWorker')