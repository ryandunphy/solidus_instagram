class CreateInstagramFeeds < ActiveRecord::Migration[5.1]
  def change
    create_table :instagram_feeds do |t|
      t.string :number
      t.string :client_id
      t.string :token
      t.boolean :enabled

      t.timestamps
    end
  end
end
