module Spree
  module Admin
    OrdersController.class_eval do

        def instagram
          @instagram = InstagramFeed.new
        end

        def create
          @instagram = InstagramFeed.new(params[:instagram])
            if @instagram.save
            redirect_to @instagram, alert: "Success!"
            else
            redirect_to new_user_path, alert: "Crap! Failure!"
            end
        end

        def user_params
          params.require(:instagram).permit(:enable, :token, :client_id, :number)
        end

        def button
          load 'InstagramWorker.rb'
        end
      end
  end
end

