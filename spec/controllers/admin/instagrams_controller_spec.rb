require 'rails_helper'

RSpec.describe Admin::InstagramsController, type: :controller do

  describe "GET #instagram" do
    it "returns http success" do
      get :instagram
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #button" do
    it "returns http success" do
      get :button
      expect(response).to have_http_status(:success)
    end
  end

end
